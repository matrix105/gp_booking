import React from 'react';

function FourOFour(props) {
    return (
        <div>
            <h1>You are not authorized to visit this page</h1>
        </div>
    );
}

export default FourOFour;